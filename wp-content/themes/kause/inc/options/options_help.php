	<div class="wrap">

		<div id="icon-themes" class="icon32"></div>

		<h2>Kause - <?php _e("Help Center", "loc_canon"); ?></h2>

		<br><br>

		<!-- TABS -->		
			<div class="tab_control">
			
				<input type="hidden" id="show_tab" name="canon_options_help[show_tab]" value="<?php if (isset($canon_options_help['show_tab'])) {echo $canon_options_help['show_tab'];} else {echo "theme_help";} ?>">

				<div class="theme_help settings_tab current_tab" data-tab="theme_help">
					<?php _e("Theme help", "loc_canon"); ?>
				</div>

				<div class="codex settings_tab last_tab" data-tab="codex">
					WordPress codex
				</div>

				<div class="tab_divider">
					<hr>
				</div>

			</div>

		<!-- THEME HELP -->		
			<div class="theme_help tab_section">


				<iframe 
					name="theme_help" 
					src="http://www.themecanon.com/docs/kause/" 
					frameborder="0" 
					scrolling="auto" 
					width="100%" 
					height="5000px" 
					marginwidth="5" 
					marginheight="5"
				>
				</iframe> 


			</div>

		<!-- CODEX -->		
			<div class="codex tab_section">

				<iframe 
					name="faq" 
					src="http://codex.wordpress.org/" 
					frameborder="0" 
					scrolling="auto" 
					width="100%" 
					height="5000px" 
					marginwidth="5" 
					marginheight="5"
				>
				</iframe> 


			</div>


	</div>

